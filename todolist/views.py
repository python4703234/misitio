from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import  Tarea
from .models import Actividad
from .forms import CrearTarea, CrearActividades
# Create your views here.
def holamundo(request):
    return HttpResponse("HELLO DJANGO WORLD")

def index(request):
    mensaje = "Este es un parametro"
    return render(request, 'index.html', {
'mensaje': mensaje
})

def acercade(request):
    return render(request, 'aboutme.html')

def pelis(request):
    return render(request, 'mymovies.html')

def task(request):
    tareas = Tarea.objects.all()
    return render(request, 'task.html', {
'task': tareas
})


def activity(request):
    actividad = Actividad.objects.all()
    print(actividad)  # Agregar este print para verificar los datos
    return render(request, 'actividad.html', {'activity': actividad})



def create_task(request):
    if request.method == 'GET':
        return render(request, 'crear_tarea.html', {
    'form': CrearTarea(),
})
    else:
        text = request.POST['texto']
    if 'completada' in request.POST:
        completada = True
    else:
        completada = False
    Tarea.objects.create(texto = text, completada=completada)
    return redirect('crear_tarea.html')



def crear_actividades(request):
    if request.method == 'GET':
        return render(request, 'crear_actividades.html', {
        'form': CrearActividades(),
    })
    else:
        activity = request.POST['actividad']
        task = Tarea.objects.get(id=request.POST['tarea'])
        Actividad.objects.create(actividad=activity, tarea= task)
        return redirect('crear_actividades.html')