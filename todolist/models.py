from django.db import models

# Create your models here.
class Tarea(models.Model):
    texto = models.CharField(max_length=200)
    completada= models.BooleanField(default=False)

    def __str__(self):
        return self.texto
    
class Actividad(models.Model):
    actividad = models.CharField(max_length=200)
    tarea = models.ForeignKey(Tarea, on_delete=models.CASCADE)

    def __str__(self):
        return self.actividad